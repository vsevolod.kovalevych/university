﻿using Robot.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kovalevych.Vsevolod.RobotChallange {
    public class KovalevychAlgorithm : IRobotAlgorithm {
        public KovalevychAlgorithm() {
            Logger.OnLogRound += Logger_OnLogRound;
        }
        private void Logger_OnLogRound(object sender, LogRoundEventArgs e) {
            ++RoundCounter;
        }
        public int RoundCounter {
            get; set;
        }
        public Position FindNearestFreeStation(Robot.Common.Robot movingRobot, Map map, IList<Robot.Common.Robot> robots) {
            EnergyStation nearest = null;
            int minDistance = int.MaxValue;
            foreach (var station in map.Stations) {
                int a = station.Position.X;
                int b = station.Position.Y;
                if (IsStationFree(station.Position, movingRobot, robots) && !AreRobotsAround(station.Position, movingRobot, robots)) {
                    int d = DistanceHelper.FindDistance(station.Position, movingRobot.Position);
                    if (d < minDistance) {
                        minDistance = d;
                        nearest = station;
                    }
                }
            }
            return nearest == null ? null : nearest.Position;
        }
        public bool AreRobotsAround(Position Pos, Robot.Common.Robot movingRobot, IList<Robot.Common.Robot> robots) {
            bool help = false;
            for (int i = -2; i < 2; ++i) {
                for (int j = -2; j < 2; ++j) {
                    Position HelpPos = new Position(Pos.X + i, Pos.Y + j);
                    if (!IsCellFree(HelpPos, movingRobot, robots) && movingRobot.Position != HelpPos) {
                        help = true;
                    }
                }
            }
            return help;
        }
        public bool IsStationFree(Position pos, Robot.Common.Robot movingRobot, IList<Robot.Common.Robot> robots) {
            if (!AreRobotsAround(pos, movingRobot, robots)) {
                return true;
            }
            else {
                int index = 0;
                foreach (var robot in robots) {
                    if (movingRobot == robot) {
                        break;
                    }
                    ++index;
                }
                int index1 = 0;
                foreach (var robot in robots) {
                    if (robot != movingRobot) {
                        if (Math.Abs(robot.Position.X - pos.X) < 3 && Math.Abs(robot.Position.Y - pos.Y) < 3) {
                            if (index < index1) {
                                return true;
                            }
                            if (robot.OwnerName.ToString() != movingRobot.OwnerName.ToString()) {
                                return true;
                            }
                            else { 
                                return false;
                            }
                        }
                    }
                    ++index1;
                }
                return true;
            }
        }
        public static bool IsCellFree(Position cell, Robot.Common.Robot movingRobot, IList<Robot.Common.Robot> robots) {
            int index = 0;
            foreach (var robot in robots) {
                if (movingRobot == robot) {
                    break;
                }
                ++index;
            }
            int index1 = 0;
            foreach (var robot in robots) {
                if (robot != movingRobot) {
                    if (robot.Position == cell && index < index1) {
                        if (robot.OwnerName.ToString() != movingRobot.OwnerName.ToString()) {
                            return true;
                        }
                        else {
                            return false;
                        }
                    }
                }
                ++index1;
            }
            return true;
        }
        public string Author {
            get {
                return "Vsevolod Kovalevych";
            }
        }
        public string Description {
            get {
                return "My Algorithm";
            }
        }
        public RobotCommand DoStep(IList<Robot.Common.Robot> robots, int robotToMoveIndex, Map map) {
            Robot.Common.Robot movingRobot = robots[robotToMoveIndex];
            if (RoundCounter == 50) {
                foreach (var robot in robots) {
                    if (DistanceHelper.FindDistance(movingRobot.Position, robot.Position) < 50) {
                        return new MoveCommand() {
                            NewPosition = robot.Position
                        };
                    }
                }
            }
            if ((movingRobot.Energy >= 220) && (robots.Count < map.Stations.Count) && RoundCounter < 13) {
                return new CreateNewRobotCommand();
            }
            Position stationPosition = FindNearestFreeStation(robots[robotToMoveIndex], map, robots);
            if (stationPosition == null) {
                return null;
            }
            if (DistanceHelper.FindDistance(stationPosition, movingRobot.Position) <= 5 && IsStationFree(stationPosition, movingRobot, robots)) {
                return new CollectEnergyCommand();
            }
            else {
                stationPosition.X -= 1;
                int distance = DistanceHelper.FindDistance(stationPosition, movingRobot.Position);
                if (movingRobot.Energy >= distance) {
                    Position newPosition = stationPosition;
                    return new MoveCommand() {
                        NewPosition = newPosition
                    };
                }
                else {
                    int SignX = Math.Sign(stationPosition.X - movingRobot.Position.X);
                    int SignY = Math.Sign(stationPosition.Y - movingRobot.Position.Y);
                    int diffX = stationPosition.X - movingRobot.Position.X;
                    int diffY = stationPosition.Y - movingRobot.Position.Y;
                    int dx = 1;
                    int dy = 1;
                    if (diffX % 2 == 0) {
                        if (SignX > 0) {
                            dx = 2;
                        }
                        if (SignX < 0) {
                            dx = -2;
                        }   
                    }
                    if (diffX % 3 == 0) {
                        if (SignX > 0) {
                            dx = 3;
                        }
                        if (SignX < 0) {
                            dx = -3;
                        }
                    }
                    if (dx == 1) {
                        if (SignX > 0) {
                            dx = 1;
                        }
                        if (SignX < 0) {
                            dx = -1;
                        }

                    }
                    if (diffY % 2 == 0) {
                        if (SignY > 0) {
                            dy = 2;
                        }
                        if (SignY < 0) {
                            dy = -2;
                        }
                    }
                    if (diffY % 3 == 0) {
                        if (SignY > 0) {
                            dy = 3;
                        }
                        if (SignY < 0) {
                            dy = -3;
                        }
                    }
                    if (diffY == 1) {
                        if (SignY > 0) {
                            dy = 1;
                        }
                        if (SignY < 0) {
                            dy = -1;
                        }
                    }
                    Position newPosition = new Position(movingRobot.Position.X + dx, movingRobot.Position.Y + dy);
                    return new MoveCommand() {
                        NewPosition = newPosition
                    };
                }
            }
        }
    }
}
