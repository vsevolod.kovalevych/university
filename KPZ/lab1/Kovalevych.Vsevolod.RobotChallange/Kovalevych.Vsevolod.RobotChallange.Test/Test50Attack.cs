﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Robot.Common;

namespace Kovalevych.Vsevolod.RobotChallange.Test
{
    [TestClass]
    public class Test50Attack
    {
        [TestMethod]
        public void TestMethod1()
        {

            var robot1 = new Robot.Common.Robot() { Energy = 100, Position = new Robot.Common.Position(6, 6) };

            var robot2 = new Robot.Common.Robot() { Energy = 100, Position = new Robot.Common.Position(4, 4) };

            var algh = new KovalevychAlgorithm();
            var station1Pos = new Position(6, 6);
            var station2Pos = new Position(4, 4);
            List<EnergyStation> stations = new List<EnergyStation>() { };
            stations.Add(new EnergyStation() { Position = station1Pos });
            stations.Add(new EnergyStation() { Position = station2Pos });
            var map = new Map()
            {
                Stations = stations
            };
            List<Robot.Common.Robot> robots = new List<Robot.Common.Robot>()
            {
                robot1,robot2,
            };
            algh.RoundCounter = 50;
            MoveCommand moveCommand = (MoveCommand)algh.DoStep(robots, 1, map);

            Assert.AreEqual(moveCommand.NewPosition, station1Pos);


        }
    }
}
