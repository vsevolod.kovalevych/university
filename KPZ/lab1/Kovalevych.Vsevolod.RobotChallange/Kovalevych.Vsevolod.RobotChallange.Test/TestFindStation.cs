﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Robot.Common;

namespace Kovalevych.Vsevolod.RobotChallange.Test
{
    [TestClass]
    public class TestFindStatiob
    {
        [TestMethod]
        public void TestMethod1()
        {
            var robot1 = new Robot.Common.Robot() { Energy = 100, Position = new Robot.Common.Position(0, 0) };
            
            var algh = new KovalevychAlgorithm();
            var station1Pos = new Position(5, 5);
            var station2Pos = new Position(8, 7);
            List<EnergyStation> stations = new List<EnergyStation>() { };
            stations.Add(new EnergyStation() { Position = station1Pos });
            stations.Add(new EnergyStation() { Position = station2Pos });
            var map = new Map()
            {
                Stations = stations
            };
            List<Robot.Common.Robot> robots = new List<Robot.Common.Robot>()
            {
                robot1
            };      
            Assert.AreEqual(algh.FindNearestFreeStation(robot1, map ,robots), station1Pos);
        }
    }
}
